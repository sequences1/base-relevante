package me.relevante.util;

import me.relevante.model.NetworkEntity;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by daniel-ibanez on 18/07/16.
 */
public class NetworkMapConverter<T extends NetworkEntity> {

    public Map<String, T> convert(Collection<T> collection) {
        Map<String, T> map = new HashMap<>();
        collection.forEach(item -> map.put(item.getNetwork().getName(), item));
        return map;
    }
}
