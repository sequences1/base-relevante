package me.relevante.api;

import me.relevante.network.Network;

public final class NetworkBasicAuthCredentials<N extends Network> extends NetworkCredentials<N> {

    private String url;
    private String username;
    private String password;

    public NetworkBasicAuthCredentials(N network,
                                       String userId,
                                       String url,
                                       String username,
                                       String password) {
        super(network, userId);
        this.url = url;
        this.username = username;
        this.password = password;
    }

    public NetworkBasicAuthCredentials() {
        super();
    }

    public String getUrl() {
        return url;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
