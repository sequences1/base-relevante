package me.relevante.api;

import me.relevante.network.Network;

public final class NetworkOAuthCredentials<N extends Network> extends NetworkCredentials<N> {

    private String oAuthConsumerKey;
    private String oAuthConsumerSecret;
    private String oAuthAccessToken;
    private String oAuthAccessSecret;

    public NetworkOAuthCredentials(N network,
                                   String userId,
                                   OAuthKeyPair<N> oAuthConsumerPair,
                                   OAuthTokenPair<N> oAuthAccessTokenPair) {
        super(network, userId);
        this.oAuthConsumerKey = oAuthConsumerPair.getKey();
        this.oAuthConsumerSecret = oAuthConsumerPair.getSecret();
        this.oAuthAccessToken = oAuthAccessTokenPair.getToken();
        this.oAuthAccessSecret = oAuthAccessTokenPair.getSecret();
    }

    public NetworkOAuthCredentials() {
        super();
    }

    public String getOAuthConsumerKey() {
        return oAuthConsumerKey;
    }

    public String getOAuthConsumerSecret() {
        return oAuthConsumerSecret;
    }

    public String getOAuthAccessToken() {
        return oAuthAccessToken;
    }

    public String getOAuthAccessSecret() {
        return oAuthAccessSecret;
    }

    public OAuthKeyPair<N> getOAuthConsumerKeyPair() {
        return new OAuthKeyPair<>(oAuthConsumerKey, oAuthConsumerSecret);
    }

    public OAuthTokenPair<N> getOAuthAccessTokenPair() {
        return new OAuthTokenPair<>(oAuthAccessToken, oAuthAccessSecret);
    }

}
