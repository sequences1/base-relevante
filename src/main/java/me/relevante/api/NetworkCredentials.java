package me.relevante.api;

import me.relevante.model.NetworkEntity;
import me.relevante.network.AbstractNetwork;
import me.relevante.network.Network;

public abstract class NetworkCredentials<N extends Network> implements NetworkEntity {

    private String network;
    private String userId;

    public NetworkCredentials() {
    }

    public NetworkCredentials(N network,
                              String userId) {
        this();
        this.network = network.getName();
        this.userId = userId;
    }

    @Override
    public Network getNetwork() {
        final String networkName = this.network;
        Network network = new AbstractNetwork(networkName) {};
        return network;
    }

    public String getUserId() {
        return userId;
    }

}
