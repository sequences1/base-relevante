package me.relevante.model;

import me.relevante.network.AbstractNetwork;
import me.relevante.network.Network;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.UUID;

@Document
public class WatchlistUser implements Serializable {

    @Id
    private String id;
    private String watchlistId;
    private String network;
    private String userId;
    private String sourcePostId;

    @Transient
    private Watchlist watchlist;

    private WatchlistUser() {}

    public WatchlistUser(String id,
                         String watchlistId,
                         Network network,
                         String userId,
                         String sourcePostId) {
        this.id = id;
        this.watchlistId = watchlistId;
        this.network = network.getName();
        this.userId = userId;
        this.sourcePostId = sourcePostId;
        this.watchlist = null;
    }

    public WatchlistUser(String watchlistId,
                         Network network,
                         String userId,
                         String sourcePostId) {
        this(UUID.randomUUID().toString(), watchlistId, network, userId, sourcePostId);
    }

    public String getId() {
        return id;
    }

    public String getWatchlistId() {
        return watchlistId;
    }

    public Network getNetwork() {
        return new AbstractNetwork(network) {};
    }

    public String getUserId() {
        return userId;
    }

    public NetworkUser getNetworkUser() {
        final String userId = getUserId();
        final Network network = getNetwork();
        return new NetworkUser() {
            @Override
            public String getUserId() {
                return userId;
            }

            @Override
            public Network getNetwork() {
                return network;
            }
        };
    }

    public String getSourcePostId() {
        return sourcePostId;
    }

    public Watchlist getWatchlist() {
        return watchlist;
    }

    public void setWatchlist(Watchlist watchlist) {
        this.watchlist = watchlist;
        if (watchlist != null) {
            this.watchlistId.equals(watchlist.getId());
        }
    }

    @Override
    public boolean equals(Object o) {
        WatchlistUser that = (WatchlistUser) o;
        if (watchlistId != null ? !watchlistId.equals(that.watchlistId) : that.watchlistId != null) return false;
        if (network != null ? !network.equals(that.network) : that.network != null) return false;
        return !(userId != null ? !userId.equals(that.userId) : that.userId != null);

    }

    @Override
    public int hashCode() {
        int result = watchlistId != null ? watchlistId.hashCode() : 0;
        result = 31 * result + (network != null ? network.hashCode() : 0);
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        return result;
    }
}
