package me.relevante.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Blacklist {

    private String relevanteId;
    private Map<String, BlacklistUser> users;

    public Blacklist(String relevanteId,
                     List<BlacklistUser> blacklistUsers) {
        this.relevanteId = relevanteId;
        this.users = new HashMap<>(blacklistUsers.size());
        for (BlacklistUser blacklistUser : blacklistUsers) {
            String key = composeKey(blacklistUser);
            this.users.put(key, blacklistUser);
        }

    }

    public String getRelevanteId() {
        return relevanteId;
    }

    public boolean isBlacklistUser(NetworkUser networkUser) {
        String key = composeKey(networkUser);
        return this.users.containsKey(key);
    }

    private String composeKey(NetworkUser networkUser) {
        return networkUser.getNetwork().getName() + "-" + networkUser.getUserId();
    }
}
