package me.relevante.model;

import me.relevante.network.Network;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;

@Document
public class UserTag {

    @Id
    private String id;
	private String relevanteId;
	private String userId;
    private String network;
	private String tag;

	private UserTag() {}

	public UserTag(String id,
                   String relevanteId,
                   Network network,
                   String userId,
                   String tag) {
        this.id = id;
        this.relevanteId = relevanteId;
        this.network = network.getName();
		this.userId = userId;
		this.tag = tag;
	}

    public UserTag(String relevanteId,
                   Network network,
                   String userId,
                   String tag) {
        this(UUID.randomUUID().toString(), relevanteId, network, userId, tag);
    }

    public String getId() {
        return id;
    }

    public String getRelevanteId() {
        return relevanteId;
    }

    public Network getNetwork() {
        return new Network() {
            @Override
            public String getName() {
                return network;
            }
        };
    }

    public String getUserId() {
        return userId;
    }

    public String getTag() {
        return tag;
    }

    public NetworkUser getNetworkUser() {
        return new NetworkUserImpl();
    }

    private class NetworkUserImpl implements NetworkUser {
        @Override
        public Network getNetwork() {
            return new Network() {
                @Override
                public String getName() {
                    return network.toLowerCase();
                }
                @Override
                public boolean equals(Object obj) {
                    return ((Network) obj).getName().equals(network.toLowerCase());
                }
            };
        }

        @Override
        public String getUserId() {
            return userId;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserTag userTag = (UserTag) o;

        if (relevanteId != null ? !relevanteId.equals(userTag.relevanteId) : userTag.relevanteId != null) return false;
        if (userId != null ? !userId.equals(userTag.userId) : userTag.userId != null) return false;
        if (network != null ? !network.equals(userTag.network) : userTag.network != null) return false;
        return !(tag != null ? !tag.equals(userTag.tag) : userTag.tag != null);

    }

    @Override
    public int hashCode() {
        int result = relevanteId != null ? relevanteId.hashCode() : 0;
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        result = 31 * result + (network != null ? network.hashCode() : 0);
        result = 31 * result + (tag != null ? tag.hashCode() : 0);
        return result;
    }
}
