package me.relevante.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Document
public class Watchlist implements Serializable {

    @Id
    private String id;
    private String relevanteId;
    private String name;
    private List<String> searchTerms;

    private Watchlist() {
        this.searchTerms = new ArrayList<>();
    }

    public Watchlist(String id,
                     String relevanteId,
                     String name,
                     List<String> searchTerms) {
        this();
        this.id = id;
        this.relevanteId = relevanteId;
        this.name = name;
        this.searchTerms.clear();
        this.searchTerms.addAll(searchTerms);
    }

    public Watchlist(String relevanteId,
                     String name,
                     List<String> searchTerms) {
        this(UUID.randomUUID().toString(), relevanteId, name, searchTerms);
    }

    public String getId() {
        return id;
    }

    public String getRelevanteId() {
        return relevanteId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getSearchTerms() {
        return searchTerms;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Watchlist watchlist = (Watchlist) o;

        if (relevanteId != null ? !relevanteId.equals(watchlist.relevanteId) : watchlist.relevanteId != null)
            return false;
        return !(name != null ? !name.equals(watchlist.name) : watchlist.name != null);

    }

    @Override
    public int hashCode() {
        int result = relevanteId != null ? relevanteId.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
