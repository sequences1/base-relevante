package me.relevante.model;

import me.relevante.api.NetworkCredentials;
import me.relevante.network.Network;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Document(collection = "RelevanteAccount")
public class RelevanteAccount {

    @Id
    private String id;
    private String name;
    private String email;
    private String password;
    private String imageUrl;
    private List<NetworkCredentials> credentials;
    private Date creationTimestamp;
    private Date lastAccessTimestamp;
    private boolean hubEnabled;
    private boolean bulkEngageEnabled;
    private long minTimeBetweenActionsInSeconds;
    private long maxTimeWithoutMonitoringInSeconds;
    private long maxMonitoredUsers;
    private List<String> actionsTimeRanges;

    public RelevanteAccount() {
        this.credentials = new ArrayList<>();
        this.actionsTimeRanges = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public <T extends Network> NetworkCredentials<T> getCredentials(T network) {
        for (NetworkCredentials credential : credentials) {
            if (credential.getNetwork().getName().equals(network.getName())) {
                return credential;
            }
        }
        return null;
    }

    public <T extends Network> void putCredentials(NetworkCredentials<T> networkCredentials) {
        NetworkCredentials<T> existingCredentials = getCredentials((T) networkCredentials.getNetwork());
        if (existingCredentials == null) {
            this.credentials.add(networkCredentials);
        }
    }

    public void removeCredentials(Network network) {
        List<NetworkCredentials> newNetworkCredentials = new ArrayList<>();
        for (NetworkCredentials credential : credentials) {
            if (!credential.getNetwork().equals(network)) {
                newNetworkCredentials.add(credential);
            }
        }
        this.credentials.clear();
        this.credentials.addAll(newNetworkCredentials);
    }

    public boolean isNetworkConnected(Network network) {
        return (getCredentials(network) != null);
    }

    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Date creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    public Date getLastAccessTimestamp() {
        return lastAccessTimestamp;
    }

    public void setLastAccessTimestamp(Date lastAccessTimestamp) {
        this.lastAccessTimestamp = lastAccessTimestamp;
    }

    public boolean isHubEnabled() {
        return hubEnabled;
    }

    public void setHubEnabled(boolean hubEnabled) {
        this.hubEnabled = hubEnabled;
    }

    public boolean isBulkEngageEnabled() {
        return bulkEngageEnabled;
    }

    public void setBulkEngageEnabled(boolean bulkEngageEnabled) {
        this.bulkEngageEnabled = bulkEngageEnabled;
    }

    public long getMinTimeBetweenActionsInSeconds() {
        return minTimeBetweenActionsInSeconds;
    }

    public void setMinTimeBetweenActionsInSeconds(long minTimeBetweenActionsInSeconds) {
        this.minTimeBetweenActionsInSeconds = minTimeBetweenActionsInSeconds;
    }

    public long getMaxTimeWithoutMonitoringInSeconds() {
        return maxTimeWithoutMonitoringInSeconds;
    }

    public void setMaxTimeWithoutMonitoringInSeconds(long maxTimeWithoutMonitoringInSeconds) {
        this.maxTimeWithoutMonitoringInSeconds = maxTimeWithoutMonitoringInSeconds;
    }

    public long getMaxMonitoredUsers() {
        return maxMonitoredUsers;
    }

    public void setMaxMonitoredUsers(long maxMonitoredUsers) {
        this.maxMonitoredUsers = maxMonitoredUsers;
    }

    public List<String> getActionsTimeRanges() {
        return actionsTimeRanges;
    }

    @Override
    public String toString() {
        return "RelevanteAccount{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", credentials=" + credentials +
                ", creationTimestamp=" + creationTimestamp +
                ", lastAccessTimestamp=" + lastAccessTimestamp +
                ", hubEnabled=" + hubEnabled +
                ", bulkEngageEnabled=" + bulkEngageEnabled +
                ", minTimeBetweenActionsInSeconds=" + minTimeBetweenActionsInSeconds +
                ", maxTimeWithoutMonitoringInSeconds=" + maxTimeWithoutMonitoringInSeconds +
                ", actionsTimeRanges=" + actionsTimeRanges +
                '}';
    }
}