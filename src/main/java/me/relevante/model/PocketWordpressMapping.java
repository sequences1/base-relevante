package me.relevante.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by daniel-ibanez on 2/08/16.
 */
@Document(collection = "PocketWordpressMapping")
public class PocketWordpressMapping {

    @Id
    private String id;
    private String relevanteId;
    private String pocketTag;
    private String wpCategory;
    private String wpHubId;

    public PocketWordpressMapping(String relevanteId,
                                  String pocketTag,
                                  String wpCategory,
                                  String wpHubId) {
        this();
        this.relevanteId = relevanteId;
        this.pocketTag = pocketTag;
        this.wpCategory = wpCategory;
        this.wpHubId = wpHubId;
    }

    private PocketWordpressMapping() {
    }

    public String getId() {
        return id;
    }

    public String getRelevanteId() {
        return relevanteId;
    }

    public String getPocketTag() {
        return pocketTag;
    }

    public String getWpCategory() {
        return wpCategory;
    }

    public String getWpHubId() {
        return wpHubId;
    }

}
