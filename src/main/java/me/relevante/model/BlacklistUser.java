package me.relevante.model;

import me.relevante.network.Network;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.UUID;

@Document
public class BlacklistUser implements NetworkUser, Serializable {

    @Id
    private String id;
	private String relevanteId;
	private String network;
	private String userId;

    private BlacklistUser() {}

	public BlacklistUser(String id,
						 String relevanteId,
						 Network network,
						 String userId) {
        this.id = id;
		this.relevanteId = relevanteId;
		this.network = network.getName();
		this.userId = userId;
	}

    public BlacklistUser(String relevanteId,
                         Network network,
                         String userId) {
        this(UUID.randomUUID().toString(), relevanteId, network, userId);
    }

    public String getId() {
        return id;
    }

	public String getRelevanteId() {
		return relevanteId;
	}

    @Override
    public Network getNetwork() {
		return new Network() {
			@Override
			public String getName() {
				return network.toLowerCase();
			}

			@Override
			public boolean equals(Object obj) {
				return ((Network) obj).getName().equals(network.toLowerCase());
			}
		};
	}

    @Override
	public String getUserId() {
		return userId;
	}


}
