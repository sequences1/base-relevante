package me.relevante.model;

import me.relevante.network.Network;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Document(collection = "RelevanteContext")
public class RelevanteContext {

    @Id
    private String relevanteId;
    private List<UserTag> userTags;
    private List<Watchlist> watchlists;
    private List<WatchlistUser> watchlistUsers;
    private List<ContactUser> contactUsers;
    private List<BlacklistUser> blacklistUsers;
    private Date lastActionDate;
    private Date lastMonitoringTimestamp;

    public RelevanteContext(String relevanteId) {
        this();
        this.relevanteId = relevanteId;
    }

    public RelevanteContext() {
        this.relevanteId = null;
        this.userTags = new ArrayList<>();
        this.watchlists = new ArrayList<>();
        this.watchlistUsers = new ArrayList<>();
        this.contactUsers = new ArrayList<>();
        this.blacklistUsers = new ArrayList<>();
    }

    public String getRelevanteId() {
        return relevanteId;
    }

    public List<UserTag> getUserTags() {
        return userTags;
    }

    public List<String> getTagNamesByUser(NetworkUser networkUser) {
        List<String> tagNames = new ArrayList<>();
        for (UserTag userTag : userTags) {
            if (userTag.getNetworkUser().equals(networkUser)) {
                tagNames.add(userTag.getTag());
            }
        }
        return tagNames;
    }

    public UserTag addUserTag(UserTag userTagToAdd) {

        if (userTagToAdd == null) {
            return null;
        }
        for (UserTag userTag : userTags) {
            if (userTag.equals(userTagToAdd)) {
                return null;
            }
        }
        userTags.add(userTagToAdd);
        return userTagToAdd;
    }

    public UserTag removeUserTag(UserTag userTagToRemove) {

        if (userTagToRemove == null) {
            return null;
        }
        for (int i = 0; i < userTags.size(); i++) {
            UserTag userTag = userTags.get(i);
            if (userTag.equals(userTagToRemove)) {
                userTags.remove(i);
                return userTagToRemove;
            }
        }

        return null;
    }

    public List<Watchlist> getWatchlists() {
        return watchlists;
    }

    public Watchlist getWatchlistById(String watchlistId) {

        for (Watchlist watchlist : watchlists) {
            if (watchlist.getId().equals(watchlistId)) {
                return watchlist;
            }
        }
        return null;
    }

    public Watchlist getWatchlistByName(String watchlistName) {

        for (Watchlist watchlist : watchlists) {
            if (watchlist.getName().equals(watchlistName)) {
                return watchlist;
            }
        }
        return null;
    }

    public Watchlist addWatchlist(Watchlist watchlistToAdd) {

        if (watchlistToAdd == null) {
            return null;
        }
        for (Watchlist watchlist : watchlists) {
            if (watchlist.equals(watchlistToAdd)) {
                return null;
            }
        }
        watchlists.add(watchlistToAdd);
        return watchlistToAdd;
    }

    public void removeWatchlistById(String watchlistId) {

        if (watchlistId == null) {
            throw new IllegalArgumentException();
        }
        List<Watchlist> newWatchlists = new ArrayList<>();
        for (Watchlist watchlist : watchlists) {
            if (!watchlist.getId().equals(watchlistId)) {
                newWatchlists.add(watchlist);
            }
        }
        this.watchlists.clear();
        this.watchlists.addAll(newWatchlists);
        List<WatchlistUser> newWatchlistUsers = new ArrayList<>();
        for (WatchlistUser watchlistUser : watchlistUsers) {
            if (!watchlistUser.getWatchlistId().equals(watchlistId)) {
                newWatchlistUsers.add(watchlistUser);
            }
        }
        this.watchlistUsers.clear();
        this.watchlistUsers.addAll(newWatchlistUsers);

    }

    public List<WatchlistUser> getWatchlistUsers() {
        return watchlistUsers;
    }

    public List<WatchlistUser> getWatchlistUsersInWatchlist(String watchlistId) {

        List<WatchlistUser> users = new ArrayList<>();
        for (WatchlistUser watchlistUser : watchlistUsers) {
            if (watchlistUser.getWatchlistId().equals(watchlistId)) {
                users.add(watchlistUser);
            }
        }
        return users;
    }

    public List<String> getWatchlistUserIdsInWatchlistByNetwork(String watchlistId,
                                                                Network network) {
        List<String> userIds = new ArrayList<>();
        for (WatchlistUser watchlistUser : watchlistUsers) {
            if (watchlistUser.getWatchlistId().equals(watchlistId) && watchlistUser.getNetwork().equals(network)) {
                userIds.add(watchlistUser.getUserId());
            }
        }
        return userIds;
    }

    public List<String> getWatchlistNamesByUser(NetworkUser networkUser) {
        Map<String, String> watchlistNamesById = new HashMap<>();
        watchlists.forEach(watchlist -> watchlistNamesById.put(watchlist.getId(), watchlist.getName()));
        List<String> watchlistNamesByUser = new ArrayList<>();
        for (WatchlistUser watchlistUser : watchlistUsers) {
            if (watchlistUser.getNetworkUser().equals(networkUser)) {
                watchlistNamesByUser.add(watchlistNamesById.get(watchlistUser.getWatchlistId()));
            }
        }
        return watchlistNamesByUser;
    }



    public WatchlistUser addWatchlistUser(WatchlistUser watchlistUserToAdd) {

        if (watchlistUserToAdd == null) {
            return null;
        }
        for (WatchlistUser watchlistUser : watchlistUsers) {
            if (!watchlistUser.getWatchlistId().equals(watchlistUserToAdd.getWatchlistId())) {
                continue;
            }
            if (watchlistUser.equals(watchlistUserToAdd)) {
                return null;
            }
        }
        watchlistUsers.add(watchlistUserToAdd);
        return watchlistUserToAdd;
    }

    public ContactUser removeContactUser(ContactUser contactUserToRemove) {

        if (contactUserToRemove == null) {
            return null;
        }
        for (int i = 0; i < contactUsers.size(); i++) {
            ContactUser contactUser = contactUsers.get(i);
            if (contactUser.equals(contactUserToRemove)) {
                contactUsers.remove(i);
                return contactUserToRemove;
            }
        }

        return null;
    }

    public boolean isContact(NetworkUser networkUser) {
        for (ContactUser contactUser : contactUsers) {
            if (contactUser.equals(networkUser)) {
                return true;
            }
        }
        return false;
    }

    public List<ContactUser> getContactUsers() {
        return contactUsers;
    }

    public ContactUser addContactUser(ContactUser contactUserToAdd) {

        if (contactUserToAdd == null) {
            return null;
        }
        for (ContactUser contactUser : contactUsers) {
            if (contactUser.equals(contactUserToAdd)) {
                return null;
            }
        }
        contactUsers.add(contactUserToAdd);
        return contactUserToAdd;
    }

    public WatchlistUser removeWatchlistUser(WatchlistUser watchlistUserToRemove) {

        if (watchlistUserToRemove == null) {
            return null;
        }
        for (int i = 0; i < watchlistUsers.size(); i++) {
            WatchlistUser watchlistUser = watchlistUsers.get(i);
            if (watchlistUser.equals(watchlistUserToRemove)) {
                watchlistUsers.remove(i);
                return watchlistUserToRemove;
            }
        }

        return null;
    }

    public void removeContactUsersByNetwork(Network network) {
        List<ContactUser> contactUsersNotInNetwork = new ArrayList<>();
        for (ContactUser contactUser : contactUsers) {
            if (contactUser.getNetwork() != network) {
                contactUsersNotInNetwork.add(contactUser);
            }
        }
        this.contactUsers = contactUsersNotInNetwork;
    }

    public List<String> getRelatedUserIdsByNetwork(Network network) {
        List<String> userIds = new ArrayList<>();
        contactUsers.forEach(contactUser -> {
            if (contactUser.getNetwork().equals(network)) {
                userIds.add(contactUser.getUserId());
            }
        });
        watchlistUsers.forEach(watchlistUser -> {
            if (watchlistUser.getNetwork().equals(network)) {
                userIds.add(watchlistUser.getUserId());
            }
        });
        return userIds;
    }

    public List<String> getBlacklistUserIdsByNetwork(Network network) {
        List<String> userIds = new ArrayList<>();
        blacklistUsers.forEach(blacklistUser -> {
            if (blacklistUser.getNetwork().equals(network)) {
                userIds.add(blacklistUser.getUserId());
            }
        });
        return userIds;
    }

    public BlacklistUser findBlacklistUserByNetworkUser(NetworkUser networkUser) {
        for (BlacklistUser blacklistUser : blacklistUsers) {
            if (blacklistUser.equals(networkUser)) {
                return blacklistUser;
            }
        }
        return null;
    }

    public List<BlacklistUser> getBlacklistUsers() {
        return blacklistUsers;
    }

    public BlacklistUser addBlacklistUser(BlacklistUser blacklistUserToAdd) {

        if (blacklistUserToAdd == null) {
            return null;
        }
        for (BlacklistUser blacklistUser : blacklistUsers) {
            if (blacklistUser.equals(blacklistUserToAdd)) {
                return null;
            }
        }
        blacklistUsers.add(blacklistUserToAdd);
        return blacklistUserToAdd;
    }

    public BlacklistUser removeBlacklistUser(BlacklistUser blacklistUserToRemove) {

        if (blacklistUserToRemove == null) {
            return null;
        }
        for (int i = 0; i < blacklistUsers.size(); i++) {
            BlacklistUser blacklistUser = blacklistUsers.get(i);
            if (blacklistUser.equals(blacklistUserToRemove)) {
                blacklistUsers.remove(i);
                return blacklistUserToRemove;
            }
        }

        return null;
    }

    public Blacklist getBlacklist() {
        return new Blacklist(relevanteId, blacklistUsers);
    }

    public Date getLastActionDate() {
        return lastActionDate;
    }

    public void setLastActionDate(Date lastActionDate) {
        this.lastActionDate = lastActionDate;
    }

    public Date getLastMonitoringTimestamp() {
        return lastMonitoringTimestamp;
    }

    public void setLastMonitoringTimestamp(Date lastMonitoringTimestamp) {
        this.lastMonitoringTimestamp = lastMonitoringTimestamp;
    }
}
